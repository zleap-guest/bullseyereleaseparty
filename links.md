## DEBIAN 11 BULLSEYE RELEASE PARTY

* [Debian Website](https://www.debian.org)
* [Release Notes](https://www.debian.org/releases/testing/releasenotes)

* [Presentation Repository](https://salsa.debian.org/zleap-guest/bullseyepresentation)


* Back to [README.md](README.md)
